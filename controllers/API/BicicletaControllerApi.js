const Bicicleta = require("../../models/bicicleta");
var bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, bicis) {
        res.status(200).json({ bicicleta: bicis });
    })
}

exports.bicicleta_create = function (req, res) {
    var bici = new Bicicleta({
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng],
    });

    Bicicleta.add(bici, function (err, newBici) {
        res.status(200).json({
            bicicleta: newBici
        })
    })
}

exports.bicicleta_update = (req, res) => {
    var bici = new Bicicleta({
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng],
    });
    var newValues=  {
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    };

    Bicicleta.updateOne(bici, newValues,(err, biciNueva) => {
        res.status(200).json({
            biciNueva: bici
        });
    });
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.code, function (error, targetBici) {
        res.status(204).send();
    });
}

exports.bicicleta_find_code = function (req, res) {
    Bicicleta.findByCode(req.body.code, function (error, targetBici) {
        res.status(200).json({
            bicicleta: targetBici
        });
    });
}