var Bicicleta  = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis((error,bicicletas)=>{
        console.log(bicicletas);
        res.render('bicicletas/index',{bicis:bicicletas})
    });  
}

exports.bicicleta_create_get = function(req,res)
{
res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req,res)
{
    var bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bicicleta.ubicacion=[req.body.lat, req.body.lng];
    Bicicleta.add(bicicleta);
    res.redirect('/bicicletas/');
}

exports.bicicleta_update_get = function(req,res)
{
var bici = Bicicleta.findById(req.params.id);
console.log(bici);
res.render('bicicletas/update',{bici});
}

exports.bicicleta_update_post = function(req,res)
{
    
    var bicicleta = Bicicleta.findById(req.params.id);
    console.log(req.param.id);
    bicicleta.color=req.body.color;
    bicicleta.id=req.body.id;
    bicicleta.modelo=req.body.modelo;
    bicicleta.ubicacion=[req.body.lat, req.body.lng];
    res.redirect('/bicicletas/')

}

exports.bicicleta_delete_post = (req,res)=>{
Bicicleta.removeById(req.body.id);
console.log(req.body.id);
res.redirect('/bicicletas/');
}