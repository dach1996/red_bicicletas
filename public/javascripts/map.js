var mymap = L.map('mapid').setView([-0.213023, -78.406949], 13);

$.ajax({
    dataType: "json",
    url: "api/bicicletas/",
    success: function (resultado) {
        resultado.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion).addTo(mymap);
        })
    }
});

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZGFjaDE5OTYiLCJhIjoiY2tmb3VzZDA1MDR6YzMwczk3cGEyZHd4ayJ9.cPBg0lUVJbgYPTpZWSVfFQ'
}).addTo(mymap);
