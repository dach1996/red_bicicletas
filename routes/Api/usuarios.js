var express = require('express');
var router = express.Router();
var usuarioControlerApi = require('../../controllers/Api/UsuarioController');
router.get('/', usuarioControlerApi.usuarioList);
router.post('/create', usuarioControlerApi.usuarioCreate);
router.post('/reservar', usuarioControlerApi.usuarioReserva);
module.exports = router;
